<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="bootstrap.min.css"/>
    <asset:stylesheet src="fonts/all.min.css"/>

    <g:layoutHead/>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-info bg-info">
    <a class="navbar-brand" href="/projet"><asset:image src="grails.svg" alt="Grails Logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
            <sec:ifLoggedIn>
                <ul class="navbar-nav mr-auto">
                    <g:each var="c" in="${grailsApplication.controllerClasses.findAll { !["Login","Logout","RestOauth","Api"].contains(it.name)}.sort {it.name} }">
                        <li class="nav-item p-3">
                            <g:link controller="${c.logicalPropertyName}" class="nav-link text-white"
                                    style="font-size: 17px;"
                            >${c.name}</g:link>
                        </li>
                    </g:each>
                </ul>
                <span class="navbar-text">
                    <g:link controller="logout" class="nav-link text-white"
                            style="font-size: 17px;">Se déconnecter</g:link>
                </span>
            </sec:ifLoggedIn>
    </div>
</nav>

    <g:layoutBody/>



    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <asset:javascript src="application.js"/>
    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="bootstrap.min.js"/>

</body>
</html>
