<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

    <nav aria-label="breadcrumb text-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="${createLink(uri: '/')}">
                    <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
                </a>
            </li>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <li class="breadcrumb-item active" aria-current="page">
                    <g:link class="create" action="create"><span class="fa fa-plus-circle"></span>&nbsp;<g:message
                      code="default.new.label" args="[entityName]"/></g:link>
                </li>
            </sec:ifAnyGranted>
        </ol>
    </nav>




        <div id="list-annonce" class="content scaffold-list" role="main">
            <div class="container">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="sortable"><a href="/projet/annonce/index?sort=title&amp;max=10&amp;order=asc">Title</a></th>
                        <th class="sortable"><a href="/projet/annonce/index?sort=description&amp;max=10&amp;order=asc">Description</a></th>
                        <th class="sortable"><a href="/projet/annonce/index?sort=price&amp;max=10&amp;order=asc">Price</a></th>
                        <th class="sortable"><a href="/projet/annonce/index?sort=illustrations&amp;max=10&amp;order=asc">Illustrations</a></th>
                        <th class="sortable"><a href="/projet/annonce/index?sort=author&amp;max=10&amp;order=asc">Author</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${annonceList}" var="annonce">
                        <tr class="even">
                            <td><a href="/projet/annonce/show/${annonce.id}">${annonce.title}</a></td>
                            <td>${annonce.description}</td>
                            <td>${annonce.price}</td>
                            <td>
                                <g:each in="${annonce.illustrations}" var="illustration">
                                    <img src="${baseUrl + illustration.filename}" style="width: 50px"/>
                                </g:each>
                            </td>
                            <td><a href="/projet/user/show/${annonce.id}">${annonce.author.username}</a></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <div class="pagination">
                    <g:paginate total="${annonceCount ?: 0}" />
                </div>
            </div>
        </div>
    </body>
</html>
