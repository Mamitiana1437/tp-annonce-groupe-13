<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#edit-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<nav aria-label="breadcrumb text-center">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="${createLink(uri: '/')}">
                <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
            </a>
        </li>
        <li class="breadcrumb-item">
            <g:link class="list" action="index"><span class="fa fa-clipboard-list"></span>&nbsp;<g:message
              code="default.list.label" args="[entityName]"/></g:link>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <g:link class="create" action="create"><span class="fa fa-plus-circle"></span>&nbsp;<g:message
              code="default.new.label" args="[entityName]"/></g:link>
        </li>
    </ol>
</nav>


<div id="edit-annonce" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.annonce}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.annonce}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:uploadForm controller="annonce" action="update" id="${annonce.id}">
        <g:hiddenField name="version" value="${this.annonce?.version}"/>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="title">Title
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="title" value="${annonce.title}" required="" id="title">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description">Description
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="description" value="${annonce.description}" required="" id="description">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price">Price
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-10">
                                <input class="form-control" type="number decimal" name="price" value="${annonce.price}" required="" min="0.0" id="price">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="illustrations">Illustrations</label>
                            <div class="col-sm-10">
                                <g:each in="${annonce.illustrations}" var="illustration" id="illustrations">
                                    <img style="width: 200px" src="${baseUrl + illustration.filename}" />
                                </g:each>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price">Upload files
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-10">
                                <input multiple type="file" name="file" class="form-control-file" id="file_upload">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="author">Author
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-10">
                                <g:select name="author.id" class="form-control" from="${userList}" optionKey="id"
                                          optionValue="username" id="author"/>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">${message(code: 'default.button.update.label', default: 'Update')}
                        </button>
                    </div>
                </div>
            </div>

        </div>


    </g:uploadForm>
</div>
</body>
</html>
