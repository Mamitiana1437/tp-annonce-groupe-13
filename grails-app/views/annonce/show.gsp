<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<a href="#show-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>


<nav aria-label="breadcrumb text-center">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="${createLink(uri: '/')}">
                <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
            </a>
        </li>
        <li class="breadcrumb-item">
            <g:link class="list" action="index"><span class="fa fa-clipboard-list"></span>&nbsp;<g:message
              code="default.list.label" args="[entityName]"/></g:link>
        </li>
        <sec:ifAnyGranted roles="ROLE_ADMIN">
            <li class="breadcrumb-item active" aria-current="page">
                <g:link class="create" action="create"><span class="fa fa-plus-circle"></span>&nbsp;<g:message
                  code="default.new.label" args="[entityName]"/></g:link>
            </li>
        </sec:ifAnyGranted>
    </ol>
</nav>


<div class="row">
    <div class="col">
        <div class="card m-5">
            <div class="card-header">
                <h4 class="ml-3">Détail d'une annonce</h4>
            </div>

            <div class="card-body">

                <div class="row">
                    <div class="col-md-4 text-left">
                        Titre :
                    </div>
                    <div class="col-md-4 text-left">
                        ${annonce.title}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Description :
                    </div>
                    <div class="col-md-4 text-left">
                        ${annonce.description}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Prix :
                    </div>
                    <div class="col-md-4 text-left">
                        ${annonce.price}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Illustrations :
                    </div>
                    <div class="col-md-4 text-left">
                        <g:each in="${annonce.illustrations}" var="illustration">
                            <img width="100" style="margin-bottom: 10px;" src="${baseUrl +
                              illustration.filename}"/>
                        </g:each>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Auteur :
                    </div>
                    <div class="col-md-4 text-left">
                        ${annonce.author.username}
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                <button type="submit"
                        class="btn btn-primary" style="width: 100%">
                    <g:link action="edit" resource="${this.annonce}" class="text-white">
                        Modifier
                    </g:link>
                </button>
            </div>
        </div>
    </div>
</div>



</body>
</html>
