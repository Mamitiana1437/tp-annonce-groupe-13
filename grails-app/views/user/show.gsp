<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<nav aria-label="breadcrumb text-center">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="${createLink(uri: '/')}">
                <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
            </a>
        </li>
        <li class="breadcrumb-item">
            <g:link class="list" action="index"><span class="fa fa-clipboard-list"></span>&nbsp;<g:message
              code="default.list.label" args="[entityName]"/></g:link>
        </li>
        <sec:ifAnyGranted roles="ROLE_ADMIN">
            <li class="breadcrumb-item active" aria-current="page">
                <g:link class="create" action="create"><span class="fa fa-plus-circle"></span>&nbsp;<g:message
                  code="default.new.label" args="[entityName]"/></g:link>
            </li>
        </sec:ifAnyGranted>
    </ol>
</nav>


<div class="row">
    <div class="col">
        <div class="card m-5">
            <div class="card-header">
                <h4 class="ml-3">Détail d'un utilisateur</h4>
            </div>

            <div class="card-body">

                <div class="row">
                    <div class="col-md-4 text-left">
                        Nom de l'utilisateur :
                    </div>
                    <div class="col-md-4 text-left">
                        ${user.username}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Mot de passe expiré :
                    </div>
                    <div class="col-md-4 text-left">
                        ${user.passwordExpired}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Compte verouillé :
                    </div>
                    <div class="col-md-4 text-left">
                        ${user.accountLocked}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Compte expiré :
                    </div>
                    <div class="col-md-4 text-left">
                        ${user.accountExpired}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-left">
                        Activé :
                    </div>
                    <div class="col-md-4 text-left">
                        ${user.enabled}
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-4 text-left">
                        Annonces :
                    </div>
                    <div class="col-md-4 text-left">
                        <g:each in="${user.annonces}" var="annonce">
                           <span class="fa fa-chevron-circle-right"></span>
                            <a class="home"
                               href="${createLink(uri: '/annonce/show/'+annonce.id)}">${annonce.title}</a> <br>
                        </g:each>
                    </div>
                </div>

            </div>
            <div class="card-footer text-muted">
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <button type="submit"
                            class="btn btn-primary" style="width: 100%">
                        <g:link action="edit" resource="${this.user}" class="text-white">
                            Modifier
                        </g:link>
                    </button>
                </sec:ifAnyGranted>
            </div>
        </div>
    </div>
</div>

</body>
</html>
