<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#edit-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>
<nav aria-label="breadcrumb text-center">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="${createLink(uri: '/')}">
                <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
            </a>
        </li>
        <li class="breadcrumb-item">
            <g:link class="list" action="index"><span class="fa fa-clipboard-list"></span>&nbsp;<g:message
                    code="default.list.label" args="[entityName]"/></g:link>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <g:link class="create" action="create"><span class="fa fa-plus-circle"></span>&nbsp;<g:message
                    code="default.new.label" args="[entityName]"/></g:link>
        </li>
    </ol>
</nav>


<div id="edit-user" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.user}" method="PUT">
        <g:hiddenField name="version" value="${this.user?.version}"/>

        <div class="row">
            <div class="col-md-12">
                <div class="card m-5">
                    <div class="card-body">

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label for="username">Username
                                    <span class="required-indicator">*</span>
                                </label>
                            </div>

                            <div class="col-md-9">
                                <input class="form-control full-width" type="text" name="username" required=""
                                       value="${user.username}" id="username">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-3">
                                <label for="passwordExpired">Mot de passe expiré</label>
                            </div>

                            <div class="col-md-9">
                                <g:radioGroup name="passwordExpired" values="[true, false]"
                                              value="${user.passwordExpired}">
                                    <p><g:message
                                            code="${it.label == "Radio true" ? "Vrai" : "Faux"}"/>: ${it.radio}</p>
                                </g:radioGroup>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-3">
                                <label for="accountLocked">Compte verouillé</label>
                            </div>

                            <div class="col-md-9">
                                <g:radioGroup name="accountLocked" values="[true, false]" value="${user.accountLocked}">
                                    <p><g:message
                                            code="${it.label == "Radio true" ? "Vrai" : "Faux"}"/>: ${it.radio}</p>
                                </g:radioGroup>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label for="accountExpired">Compte expiré</label>
                            </div>

                            <div class="col-md-9">
                                <g:radioGroup name="accountExpired" values="[true, false]"
                                              value="${user.accountExpired}">
                                    <p><g:message
                                            code="${it.label == "Radio true" ? "Vrai" : "Faux"}"/>: ${it.radio}</p>
                                </g:radioGroup>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label for="enabled">Compte activé</label>
                            </div>

                            <div class="col-md-9">
                                <g:radioGroup name="enabled" values="[true, false]" value="${user.enabled}">
                                    <p><g:message
                                            code="${it.label == "Radio true" ? "Vrai" : "Faux"}"/>: ${it.radio}</p>
                                </g:radioGroup>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Annonces</label>
                            </div>

                            <div class="col-md-9">
                                <ul></ul><a href="/projet/annonce/create?user.id=">Add Annonce</a>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer text-muted">
                        <g:submitButton name="create" class="btn btn-primary btn-lg btn-block"
                                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                    </div>
                </div>
            </div>
        </div>
    </g:form>
</div>
</body>
</html>



