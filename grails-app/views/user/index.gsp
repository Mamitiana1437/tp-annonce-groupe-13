<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

    <nav aria-label="breadcrumb text-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="${createLink(uri: '/')}">
                    <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
                </a>
            </li>
            <li class="breadcrumb-item">
                <g:link class="create" action="create"><span class="fa fa-plus-circle"></span>&nbsp;<g:message
              code="default.new.label" args="[entityName]" /></g:link>
            </li>
        </ol>
    </nav>

        <div id="list-user" class="content scaffold-list" role="main">
            <div class="container">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="sortable" scope="col">
                            <a href="/projet/user/index?sort=username&amp;max=10&amp;order=asc">Nom d'utilisateur</a>
                        </th>
                        <th class="sortable" scope="col">
                            <a href="/projet/user/index?sort=passwordExpired&amp;max=10&amp;order=asc">Mot de passe expiré</a>
                        </th>
                        <th class="sortable" scope="col">
                            <a href="/projet/user/index?sort=accountLocked&amp;max=10&amp;order=asc">Compte verrouillé</a>
                        </th>
                        <th class="sortable" scope="col">
                            <a href="/projet/user/index?sort=accountExpired&amp;max=10&amp;order=asc">Compte expiré</a>
                        </th>
                        <th class="sortable" scope="col">
                            <a href="/projet/user/index?sort=enabled&amp;max=10&amp;order=asc">Activé</a>
                        </th>
                        <th>Annonces</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${userList}" var="user">
                        <tr class="even">
                            <td class="text-center"><a href="/projet/user/show/${user.id}">${user.username}</a></td>
                            <td class="text-center">${user.passwordExpired}</td>
                            <td class="text-center">${user.accountLocked}</td>
                            <td class="text-center">${user.accountExpired}</td>
                            <td class="text-center">${user.enabled}</td>
                            <td class="p-5">
                                <ul>
                                    <g:each in="${user.annonces}" var="annonce">
                                        <span class="fa fa-chevron-circle-right"></span>
                                                <a href="/projet/annonce/show/${annonce.id}">${annonce.title}</a>
                                        <br>
                                    </g:each>
                                </ul>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="pagination">
                    <g:paginate total="${userCount ?: 0}" />
                </div>
            </div>
            %{--<h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${userList}" />--}%
        </div>
    </body>
</html>
