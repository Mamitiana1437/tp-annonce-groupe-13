<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#create-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <nav aria-label="breadcrumb text-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${createLink(uri: '/')}">
                        <span class="fa fa-home"></span>&nbsp;<g:message code="default.home.label"/>
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <g:link class="list" action="index"><span class="fa fa-clipboard-list"></span>&nbsp;<g:message
                      code="default.list.label" args="[entityName]"/></g:link>
                </li>
            </ol>
        </nav>


        <div id="create-user" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.user}" method="POST">
                <div class="row">
                   <div class="col-md-12">
                       <div class="card m-5">
                           <div class="card-body">
                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="password">Password
                                           <span class="required-indicator">*</span>
                                       </label>
                                   </div>
                                   <div class="col-md-9">
                                       <input class="form-control full-width" type="password" name="password" required="" value="" id="password">
                                   </div>
                               </div>

                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="username">Username
                                           <span class="required-indicator">*</span>
                                       </label>
                                   </div>
                                   <div class="col-md-9">
                                       <input class="form-control full-width" type="text" name="username" required=""
                                              value="" id="username">
                                   </div>
                               </div>


                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="passwordExpired">Password Expired</label>
                                   </div>
                                   <div class="col-md-9">
                                       <input type="hidden" name="_passwordExpired"><input type="checkbox"
                                                                                           name="passwordExpired"
                                                                                           id="passwordExpired">
                                   </div>
                               </div>


                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="accountLocked">Account Locked</label>
                                   </div>
                                   <div class="col-md-9">
                                       <input type="hidden" name="_accountLocked">
                                       <input type="checkbox"
                                              name="accountLocked"
                                              id="accountLocked">
                                   </div>
                               </div>

                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="accountExpired">Account Expired</label>
                                   </div>
                                   <div class="col-md-9">
                                       <input type="hidden" name="_accountExpired">
                                       <input type="checkbox"
                                              name="accountExpired"
                                              id="accountExpired">
                                   </div>
                               </div>

                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="enabled">Enabled</label>
                                   </div>
                                   <div class="col-md-9">
                                       <input type="hidden" name="_enabled">
                                       <input type="checkbox"
                                              name="enabled"
                                              checked="checked"
                                              id="enabled">
                                   </div>
                               </div>

                               <div class="form-group row">
                                   <div class="col-md-3">
                                       <label for="annonces">Annonces</label>
                                   </div>
                                   <div class="col-md-9">
                                       <ul></ul><a href="/projet/annonce/create?user.id=">Add Annonce</a>
                                   </div>
                               </div>

                           </div>
                           <div class="card-footer text-muted">
                               <g:submitButton name="create" class="btn btn-primary btn-lg btn-block" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                           </div>
                       </div>
                   </div>
                </div>
            </g:form>
        </div>
    </body>
</html>
