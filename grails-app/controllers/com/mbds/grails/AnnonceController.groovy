package com.mbds.grails

import grails.core.GrailsApplication
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.grails.core.io.ResourceLocator

import static org.springframework.http.HttpStatus.*

@Secured('ROLE_ADMIN')
class AnnonceController {
    def assetResourceLocator
    AnnonceService annonceService
    @Secured(['ROLE_MODO','ROLE_ADMIN'])



    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond annonceService.list(params), model:[annonceCount: annonceService.count(), baseUrl: grailsApplication.config.annonces.illustrations.url]
    }

    @Secured(['ROLE_MODO','ROLE_ADMIN'])
    def show(Long id) {
        respond annonceService.get(id), model: [baseUrl: grailsApplication.config.annonces.illustrations.url]

    }

    def create() {
        respond new Annonce(params), model: [userList: User.list()]
    }

    def save(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }
        HashSet illustrations = new HashSet<>()
        try{
            def downloadfiles = request.getFiles("file")
            if(downloadfiles!=null){
                String pathImage = grailsApplication.config.annonces.illustrations.path.toString()
                println "absolutePath" + pathImage



                downloadfiles.each {
                    downloadfile ->
                        String filename = downloadfile.getOriginalFilename()
                        println "pathImage:" + (pathImage + File.separatorChar + filename)
                        downloadfile.transferTo(new File(pathImage + File.separatorChar + filename))
                        Illustration illu=new Illustration();
                        illu.annonce = annonce
                        illu.filename =   filename
                        if(!filename.isEmpty())
                            illustrations.add(illu)
                }

            }
        }catch(Exception exc){
            exc.printStackTrace()
        }
        try {
            annonce.illustrations =illustrations
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }
    @Secured(['ROLE_MODO','ROLE_ADMIN'])
    def edit(Long id) {
        def userList = User.list()
        respond annonceService.get(id), model: [userList: userList, baseUrl: grailsApplication.config.annonces.illustrations.url]
    }

    def update() {
        def annonce = Annonce.get(params.id)
        annonce.title = params.title
        annonce.description = params.description
        annonce.price = Double.parseDouble(params.price)
        annonce.author = User.get(params.author.id)
        if (annonce == null) {
            notFound()
            return
        }
        HashSet illustrations = new HashSet<>()
        try{
            def downloadfiles = request.getFiles("file")
            if(downloadfiles!=null){
                    String pathImage = grailsApplication.config.annonces.illustrations.path.toString()
                    println "absolutePath" + pathImage



                    downloadfiles.each {
                        downloadfile ->
                            String filename = downloadfile.getOriginalFilename()
                            println "pathImage:" + (pathImage + File.separatorChar + filename)
                            downloadfile.transferTo(new File(pathImage + File.separatorChar + filename))
                            Illustration illu=new Illustration();
                            illu.annonce = annonce
                            illu.filename =   filename
                            if(!filename.isEmpty())
                                illustrations.add(illu)
                    }

            }
        }catch(Exception exc){
            exc.printStackTrace()
        }

        /**
         * 1. Récupérer le fichier dans la requête
         * 2. Sauvegarder le fichier localement
         * 3. Créer un illustration sur le fichier que vous avez sauvegardé
         * 4. Attacher l'illustration nouvellement créée à l'annonce
         */

        try {
            if(!illustrations.isEmpty()){
                annonce.illustrations.clear()
                annonce.illustrations.addAll(illustrations)
            }


            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'annonce.label', default: 'Annonce'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
