package com.mbds.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

@Secured('ROLE_ADMIN')
class ApiController {

    AnnonceService annonceService
    UserService userService

//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                /*response.withFormat {
                    xml { render annonceInstance as XML }
                    json { render annonceInstance as JSON }
                }*/
                serializeData(annonceInstance, request.getHeader("Accept"))
                break
            case "PUT":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                annonceInstance.setTitle(request.getJSON().title)
                annonceInstance.setDescription(request.getJSON().description)
                annonceInstance.setPrice(request.getJSON().price)
                annonceInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            case "POST":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                annonceInstance.setTitle(params.title)
                annonceInstance.setDescription(params.description)
                annonceInstance.setPrice(Double.parseDouble(params.price))
                def author = userService.get(params.authorId)
                if(author!=null){
                    annonceInstance.author = author
                }
                HashSet illustrations = new HashSet<>()
                try{
                    def downloadfiles = request.getFiles("file")
                    if(downloadfiles!=null){
                        String pathImage = grailsApplication.config.annonces.illustrations.path.toString()
                        println "absolutePath" + pathImage



                        downloadfiles.each {
                            downloadfile ->
                                String filename = downloadfile.getOriginalFilename()
                                println "pathImage:" + (pathImage + File.separatorChar + filename)
                                downloadfile.transferTo(new File(pathImage + File.separatorChar + filename))
                                Illustration illu=new Illustration();
                                illu.annonce = annonceInstance
                                illu.filename =   filename
                                if(!filename.isEmpty())
                                    illustrations.add(illu)
                        }

                    }
                }catch(Exception exc){
                    exc.printStackTrace()
                }
                if(!illustrations.isEmpty()){
                    annonceInstance.illustrations.clear()
                    annonceInstance.illustrations.addAll(illustrations)
                }
                annonceInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            case "PATCH":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                if (request.getJSON().title)
                    annonceInstance.setTitle(request.getJSON().title)
                if (request.getJSON().description)
                    annonceInstance.setDescription(request.getJSON().description)
                if (request.getJSON().price)
                    annonceInstance.setDescription(request.getJSON().price)

                annonceInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                annonceInstance.delete(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
//    localhost:8081/projet/api/annonces?limit
    def annonces() {
        switch (request.getMethod()) {
            case "GET":
                def p = [
                        max: Math.min(Integer.parseInt(params.max) ?: 10, 100),
                        offset: 0
                ]
                def annonces = annonceService.list(p)
                serializeData(annonces, request.getHeader("Accept"))
                break
            case "POST":
                def authorInstance = User.get(params.authorId)
                def annonceInstance = new Annonce()
                annonceInstance.setTitle(params.title)
                annonceInstance.setDescription(params.description)
                annonceInstance.setPrice(Double.parseDouble(params.price))
                HashSet illustrations = new HashSet<>()
                try{
                    def downloadfiles = request.getFiles("file")
                    if(downloadfiles!=null){
                        String pathImage = grailsApplication.config.annonces.illustrations.path.toString()
                        println "absolutePath" + pathImage



                        downloadfiles.each {
                            downloadfile ->
                                String filename = downloadfile.getOriginalFilename()
                                println "pathImage:" + (pathImage + File.separatorChar + filename)
                                downloadfile.transferTo(new File(pathImage + File.separatorChar + filename))
                                Illustration illu=new Illustration();
                                illu.annonce = annonceInstance
                                illu.filename =   filename
                                if(!filename.isEmpty())
                                    illustrations.add(illu)
                        }

                    }
                }catch(Exception exc){
                    exc.printStackTrace()
                }
                if(!illustrations.isEmpty()){
                    annonceInstance.illustrations = illustrations

                }
                authorInstance.addToAnnonces(annonceInstance)
                authorInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_CREATED
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / PUT / PATCH / DELETE
    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                serializeData(userInstance, request.getHeader("Accept"))
                break
            case "PUT":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                userInstance.setUsername(request.getJSON().username)
                userInstance.setEnabled(request.getJSON().enabled)
                userInstance.setAccountExpired(request.getJSON().accountExpired)
                userInstance.setAccountLocked(request.getJSON().accountLocked)
                userInstance.setPasswordExpired(request.getJSON().passwordExpired)
                userInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            case "PATCH":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                if (request.getJSON().username)
                    userInstance.setUsername(request.getJSON().username)
                if (request.getJSON().enabled)
                    userInstance.setEnabled(request.getJSON().enabled)
                if (request.getJSON().accountExpired)
                    userInstance.setAccountExpired(request.getJSON().accountExpired)
                if (request.getJSON().accountLocked)
                    userInstance.setAccountLocked(request.getJSON().accountLocked)
                if (request.getJSON().passwordExpired)
                    userInstance.setPasswordExpired(request.getJSON().passwordExpired)
                userInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                def userRoleInstance = UserRole.findByUser(userInstance)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                userRoleInstance.delete(flush: true, failOnError: true)
                userInstance.delete(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                def p = [
                        max: Math.min(Integer.parseInt(params.max) ?: 10, 100),
                        offset: 0
                ]
                def users = userService.list(p)
                serializeData(users, request.getHeader("Accept"))
                break
            case "POST":
                def userInstance = new User(
                        username: request.getJSON().username,
                        password: request.getJSON().password
                )
                def userSaved = userInstance.save(flush: true, failOnError: true)

                def roleInstance = Role.get(request.getJSON().roleId)

                def userRoleInstance = new UserRole(
                        role: roleInstance,
                        user: userSaved
                )

                userRoleInstance.save(flush: true, failOnError: true)

                return response.status = HttpServletResponse.SC_CREATED
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

    def serializeData(object, format)
    {
        switch (format)
        {
            case 'json':
            case 'application/json':
            case 'text/json':
                render object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                render object as XML
                break
        }
    }
}
